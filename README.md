# Dropping-fruit-rl

Unzip the dist.zip folder and select main.exe to start the program.

Instructions to Start Game:

1. Select random or existing game version
2. Input map file name if playing with existing map. Must include .csv at the end (test_experience.csv).
3. Input memory file name if using existing memory. Must include .csv at the end (test_experience.csv).
4. Select agent to play (h/r).
5. If test experience is found, choose y/n if user wants to use previous test experience.

The Game:

After the game has been successfully configured, the board should look like this below.

Row: 3                           [3.0, 3.0, 0.0, -1.0, -1.0]
Row: 2                           [2.0, 4.0, 0.0, -1.0, -1.0]
Row: 1                             [0.0, 0.0, 'X', 0.0, 0.0]
Action===============================================Command
Move Right:================================================1
Move Left:================================================-1
Don't Move:================================================0
Show Current Game:=========================================v
Show Instructions:=========================================i
Show Experience Replay:====================================r
Quit:======================================================q


Move Right: The 'X' will move right 1 space.
Move Left: The 'X' will move left 1 space.
Don't Move: The 'X' will remain in the same location.
Show Current Game: The current game board will be displayed.
Show Instructions: The commands will be displayed.
Show Experience Replay: The current captured experience will be displayed.
Quit: The game will exit.

After the game:

The user will be asked if he/she will want to preserve memory. 

If yes, then enter name of csv to preserve the memory.

Retrieving Maps/Memory:
In your extracted dist.zip you will have an asset folder. The 'maps' folder will contain the current created maps.
The 'experience_data' folder will contain the memory_replay csvs.