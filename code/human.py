# -*- coding: utf-8 -*-
"""
Created on Wed Jan  1 19:39:16 2020

@author: Dingie
"""

import numpy as np
from memory_data import Experience_Replay

class Human:
    
    map_name = None
    game_map = None
    width = None
    height = None
    vision = None
     
    is_game_ongoing = True

    current_state = None
    current_action = None
    current_reward = None
    next_state = None
    
    current_position = None
    current_position_array = []
    next_position = None
    next_position_array = []
    
    current_field = []
    next_field = []

    current_row = None
    

    

    capture_current_position = []
    capture_current_field = []
    capture_next_field = []
    capture_reward = None
    capture_action = None
    memory = None
    total_reward = 0
    
    def __init__(self, map_name, game_map, width, height, vision, memory):
        print("Configuring Human agent...")
        self.map_name = map_name
        self.game_map = game_map
        self.width = width
        self.height = height
        self.vision = vision
        self.current_row = 0
        self.memory = memory
        self.configure_simulation()
        
    def configure_simulation(self):
        self.configure_agent()
        self.configure_start_field()
        
    def configure_agent(self):
        self.configure_start_position()

    def configure_start_field(self):
        self.current_field = []
        for i in range(self.vision):
            self.current_field.append(self.game_map[i])
        self.print_current_board()

    def configure_start_position(self):
        print("Setting Human start position...")
        self.current_position = (self.width//2)
        self.next_position = self.current_position
        temp_current_position_array = []
        
        for i in range(self.width):
            if (self.current_position == i):
                temp_current_position_array.append(1)
            else:
                temp_current_position_array.append(0)
        
        self.current_position_array = temp_current_position_array
        #self.print_current_position()
        
    def player_input(self, input_value):
        if (input_value == '-1'):
            print("Human agent moves left one space.")
            self.configure_state(-1)
        if (input_value == '0'):
            print("Human agent stays in same space.")
            self.configure_state(0)
        if (input_value == "1"):
            print("Human agent moves right one space.")
            self.configure_state(1)
        if (input_value == "v"):
            print("Displaying current agent position and field.")
            self.show_game()
        if (input_value == "q"):
            print("Exiting game...")
            self.set_is_game_ongoing(False)
        if (input_value =="i"):
            print("Showing command list.")
            self.print_turn_message()
        if (input_value =="r"):
            print("Showing Experience Replay.")
            self.print_experience_replay()
        return None
    
    
    def configure_state(self,input_action):
        self.configure_position(input_action)
        self.configure_field()
        self.configure_reward()
        self.capture_current_experience()
        self.set_is_game_ongoing(not self.check_end_game())
        
        return None
    
    def capture_current_experience(self):
        #print("Captured Position: ", self.current_position_array)
        #print("Captured Current State: ", self.capture_current_field)
        #print("Captured Action: ", self.capture_action)
        #print("Captured Reward: ", self.capture_reward)
        #print("Captured Next State: ", self.capture_next_field)
        
        current_experience = []
        #list(map(float,(self.current_position_array)))
        
        configured_current_field, configured_next_field = self.configure_current_experience()
        
        current_experience.append(list(map(float,(self.current_position_array))))
        
        
        current_experience.append(configured_current_field)
        current_experience.append(float(self.capture_action))
        current_experience.append(float(self.capture_reward))
        current_experience.append(configured_next_field)
        #print("current experience: ", current_experience)
        self.memory.push_experience(current_experience)
        #self.memory.print_experience_replay_array()
    
    def configure_current_experience(self):
        configured_current_field = []
        configured_next_field = []
        for i in range(len(self.capture_current_field)):
            configured_current_field.append(list(map(float,(self.capture_current_field[i]))))
            configured_next_field.append(list(map(float,(self.capture_next_field[i]))))
        return configured_current_field, configured_next_field
    
    def configure_position(self, input_action):
        if (self.current_position + input_action >= self.width-1):
            self.next_position = self.width -1 
            
        elif(self.current_position + input_action <= 0):
            self.next_position = 0
        else:
            self.next_position = self.current_position +  input_action
            
        self.next_position_array = np.zeros(self.width)
        self.next_position_array[self.next_position] = 1
        self.current_position_array = self.next_position_array
        self.capture_current_position = self.current_position
        self.capture_action = input_action
        
        self.current_position = self.next_position
        print(self.current_position)
        print(self.next_position_array)
        
    def configure_field(self):
        self.capture_current_field = self.current_field
        self.current_row += 1
        self.next_field = []
        if (self.current_row + self.vision <= self.height):
            for i in range(self.current_row, self.current_row + self.vision):
                self.next_field.append(self.game_map[i])
        elif(self.current_row + self.vision > self.height):
            zero_row_needed = self.current_row + self.vision - self.height
            for i in range(self.current_row, self.height):
                self.next_field.append(self.game_map[i])
            
            for i in range(zero_row_needed):
                self.next_field.append(np.zeros(self.width, dtype='float64'))
        #print(self.current_field)
        #print(self.next_field)
        self.capture_next_field = self.next_field
        self.current_field = self.next_field
    
    def configure_reward(self):
        #print(self.next_position)
        self.capture_reward = str(self.capture_next_field[0][self.next_position])
        self.total_reward = self.total_reward + float(self.capture_reward)
    def check_end_game(self):
        if (self.height <= self.current_row):
            print("Game has ended in: " + str(self.current_row) + " steps.")
            return True
        return False
    
    def print_map(self):
        return None
    
    def print_position(self):
        return None

    def get_vision(self):
        return None        
    

    def get_current_position(self):
        return self.current_position
    
    def set_current_position(self, new_position):
        self.current_position = new_position
    
    def get_current_state(self):
        return self.current_state
    
    def set_current_state(self, new_current_state):
        self.current_state = new_current_state  
    
    def get_current_action(self):
        return self.current_action
    
    def set_current_action(self, new_action):
        self.current_action = new_action
        
    def get_current_reward(self):
        return self.current_reward
    
    def set_current_reward(self, new_reward):
        self.current_reward = new_reward
    
    def get_next_state(self):
        return self.next_state
    
    def set_next_state(self, new_next_state):
        self.next_state = new_next_state
    
    def choose_action(self, action_selected):
        return None
    
    def calculate_action(self):
        return None
    
    def get_experience_replay(self):
        return None
    
    def get_is_game_ongoing(self):
        return self.is_game_ongoing

    def set_is_game_ongoing(self, is_game_ongoing):
        self.is_game_ongoing = is_game_ongoing
        
    def get_total_reward(self):
        return self.total_reward
    
    def show_game(self):
        #self.print_current_position()
        
        #self.print_current_field()
        self.print_reward()
        self.print_current_board()
        return None
    
    ##### PRINT DEF #####
    def print_turn_message(self):    
        format_string = "{:=<30s}{:=>30s}"    
        print(format_string.format("Action","Command"))
        print(format_string.format("Move Right:","1"))
        print(format_string.format("Move Left:","-1"))
        print(format_string.format("Don't Move:","0"))
        print(format_string.format("Show Current Game:","v"))
        print(format_string.format("Show Instructions:","i"))
        print(format_string.format("Show Experience Replay:", "r"))
        print(format_string.format("Quit:","q"))
    
    #Used for validation purpose
    def print_current_position(self):
        format_string = "{:=<30s}{:=>30s}"
        print(format_string.format("Current Position",str(self.current_position_array)))
    
    #Used for validation purpose
    def print_current_field(self):
        format_string = "{:=<30s}{:=>30s}"
        
        for i in range(len(self.current_field)):
            print(format_string.format(str(i),str(self.current_field[i])))
    
    def print_current_board(self):
        format_string = "{:<30s}{:>30s}"
        counter = 0
        for i in range(len(self.current_field)-1, -1, -1):
            if (i > 0):
                print(format_string.format("Row: "+ str(self.current_row +self.vision - counter),str(list(map(float,(self.current_field[i]))))))
                counter +=1 
            elif (i == 0):
                position_x_array = []
                for i in range(self.width):
                    if (self.current_position_array[i] == 0):
                        position_x_array.append(float(self.current_field[0][i]))
                    elif(self.current_position_array[i] == 1):
                        position_x_array.append('X')
                print(format_string.format("Row: "+ str(self.current_row +self.vision - counter),str(position_x_array)))

    def print_reward(self):
        format_string = "{:=<30s}{:=>30s}"
        print(format_string.format("Current Total Reward:", str(self.total_reward)))
        
    def print_experience_replay(self):
        print("Current Experience Replay Stored:")
        print(self.memory.get_experience_replay_array())