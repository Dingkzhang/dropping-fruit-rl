# -*- coding: utf-8 -*-
"""
Created on Wed Jan  1 19:39:10 2020

@author: Dingie
"""

import pandas as pd
import numpy as np
from datetime import datetime
from os import path

class Map_Gen:
    
    INIT_PATH = "../asset/maps/"    

    found_map = None
    default_width = 5
    min_width = 3
    max_width = 7
    
    default_height = 30
    min_height = 10
    max_height = 100
    
    min_vision = 3
    max_vision = 8
    
    min_reward = -5
    max_reward = 5
    
    map_name = None
    map_type = None    
    game_map = None
    width = None
    height = None
    vision = None
    
    def __init__(self, map_type="existing", width=5, height=30, vision=5, map_name="default"):        
        print("Configuring map settings...")
        self.configure_map(map_type, width, height, map_name)
        self.vision = vision
    
    def configure_map(self, map_type, width, height, map_name):
        print("Getting map from selection...")
        if (map_type == "existing"):
            
            if (path.exists(self.INIT_PATH + map_name) and map_name != ""):
                print("Map name found. Generating map...")
                self.game_map, self.height, self.width = self.retrieve_map(map_name)
                self.game_map = self.game_map.astype('float64')
                self.map_name = map_name
                self.found_map = True
            else:
                print("Map name not found.")
                self.found_map = False
        elif(map_type == "random"):
            self.set_map_parameter(width,height)
            self.generate_map()
            self.gen_map_name()
            self.check_to_save()
            self.found_map = True
        else:
            print("Unrecognized input value. Exiting game.")
            self.found_map = False
        self.map_type = map_type
        
    def set_map_parameter(self, width, height):
        if (width > self.max_width or width < self.min_width):
            self.width = self.default_width
        else:
            self.width = width
        
        if (height > self.max_height or height < self.min_height):
            self.height = self.default_height
        else:
            self.height = height
    
    def gen_map_name(self):
        now = datetime.now()
        save_name = now.strftime("%Y%m%d%H%M%S")
        print("Name of map file: ", save_name)
        self.map_name = save_name
    
    def check_to_save(self):
        selected_option = False
        while (selected_option == False):
            print("Do you want to save the map? y/n")
            is_saved = input()
            if (is_saved == "y"):
                print("Map is saved")
                self.save_map()
                selected_option = True
            elif(is_saved == "n"):
                print("Map is not saved")
                selected_option = True  
                
    def save_map(self):
          np.savetxt("../asset/maps/"+self.map_name+".csv",self.game_map, delimiter=",")
          print("Setting map name to: ", self.map_name)
    
    def retrieve_map(self, map_name):
        print("Retrieving existing map...")       
        map_data = pd.read_csv("../asset/maps/"+map_name, header=None)
        return map_data.to_numpy(), map_data.to_numpy().shape[0], map_data.to_numpy().shape[1]
    
    def generate_map(self):
        print("Generating random map...")
        self.game_map = np.random.uniform(self.min_reward, self.max_reward, (self.height, self.width))
        print(self.game_map)
    
    def print_map_info(self):
        print("Displaying generated information")
        print("Map Name: ", self.map_name)
        print("Map Type: ", self.map_type)
        print("Map Height: ", self.height)
        print("Map Width: ", self.width)

    def print_map(self):
        print("Game Map:")
        print(self.game_map)

    def get_map_info(self):
        return self.map_name, self.map_type, self.game_map, self.width, self.height, self.vision

    def get_found_map(self):
        return self.found_map