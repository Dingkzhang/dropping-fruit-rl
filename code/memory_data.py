# -*- coding: utf-8 -*-
"""
Created on Wed Jan  1 19:39:24 2020

@author: Dingie
"""

import os.path
from os import path
import pandas as pd

import csv

class Experience_Replay:
    
    max_capacity = 1000
    current_capacity = None
    INIT_PATH = "../asset/experience_data/"

    map_name = None
    save_name = None
    vision = None
    width = None
    experience_replay_array = []
    
    def __init__(self, map_name, vision, width, save_name):
        self.map_name = map_name
        self.save_name = save_name 
        self.vision = vision
        self.width = width
        saved_experience_exist = self.check_experience_exist()
        self.use_saved_experience(saved_experience_exist)
    
    def check_experience_exist(self):

        print("Looking for experience for map_name: " + self.save_name)
        
        print("Experience search result for map_name: " + str(path.exists(self.INIT_PATH+self.save_name)))
                
        # need to change this to map_name after testing is completed
        return (path.exists(self.INIT_PATH +self.save_name))
        #temp = pd.read_csv(self.INIT_PATH + "test_experience.csv")
    
    def use_saved_experience(self, saved_experience_exist):
        if (saved_experience_exist):
            saved_experience_choice = input("Saved experience found. Does user want to use saved experience? (y/n)")
            if (saved_experience_choice == "y"):
                print("User chose to use previous data...")
                print("Fetching experience data from storage...")
                self.get_experience_data()
            else:
                print("User chose to start with new data...")
        else:
            print("Saved experience not found. Starting with empty data array")

    def get_experience_data(self):
         with open(self.INIT_PATH+self.save_name) as csvfile:
            readCSV = csv.reader(csvfile, delimiter=",")
            state_vision_counter = 0
            temp_experience_array = []
            temp_state_array = []
            for row in readCSV:
                #print(row)
                #print(row[0])
                if (row[0] == 'B'):
                    print("Starting experience retrieval...")
                #if (row[0] == 'I'):
                #    temp_experience_array = []
                #    print("Located experience ID: " + row[1])
                #    temp_experience_array.append(float(row[1]))
                #    print("Added experience ID: " + row[1] + " to temp experience array")
                if (row[0] == 'CP'):
                    print("Located current position: " + row[1])
                    temp_experience_array.append(list(map(float,(row[1:]))))
                    print("Added current position to temp experience array")
                if (row[0] == 'CS'):
                    print("Located current state...")
                    state_vision_counter+= 1
                    temp_state_array.append(list(map(float,(row[1:]))))
                    print("Added current state to temp state array")
                    if (state_vision_counter == self.vision):
                        temp_experience_array.append(temp_state_array)
                        temp_state_array = []
                        state_vision_counter = 0
                        print("Added current state to temp experience array")

                if (row[0] == 'A'):
                    print("Located action state...")
                    temp_experience_array.append(float(row[1]))
                    print("Added action state to temp experience array")
                if(row[0] == 'R'):
                    print("Located reward state...")
                    temp_experience_array.append(float(row[1]))
                    print("Added reward state to temp experience array")
                if (row[0] == 'NS'):
                    print("Located next state...")
                    state_vision_counter+= 1
                    temp_state_array.append(list(map(float,(row[1:]))))
                    print("Added next state to temp state array")
                    if (state_vision_counter == self.vision):
                        temp_experience_array.append(temp_state_array)
                        temp_state_array = []
                        state_vision_counter = 0
                        self.experience_replay_array.append(temp_experience_array)
                        temp_experience_array = []
                        print("Added next state to temp experience array")
                
            print("Finished adding existing experience replay to experience_replay_array")
    def push_experience(self, new_experience):
        self.experience_replay_array.append(new_experience)
    
    def pop_experience(self):
        self.experience_replay_array.pop(0)
    
    def save_file(self):
        return None
    
    def delete_file(self):
        return None
    def create_csv(self):
#        with open(self.map_name+'.csv', 'wb') as csvfile:
 #           filewriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        return None          
        
    def convert_to_csv(self):
        return None
    
    def save_configuration(self):
        
        is_name_checked = False
        while (not is_name_checked):
            is_name_checked, save_file_name = self.create_save_file_name()
            self.create_experience_replay_file(save_file_name)

    
    def create_save_file_name(self):
        
        is_name_valid = False
        is_name_available = False
        while (not is_name_valid or not is_name_available):
            
            print("Please enter save file name. (Alphanumeric from 3-12 characters)")
            save_file_name = input("Enter Name: ")
            is_name_valid = self.check_save_name_valid(save_file_name)
            if (is_name_valid): 
                is_name_available = self.check_save_name_available(save_file_name)
            
        return True, save_file_name
    def check_save_name_valid(self, save_file_name):
        if(save_file_name.isalnum() and len(save_file_name) >= 3 and len(save_file_name) <= 12):
            print("Name is valid.")
        else:
            print("Name is invalid. Please enter a name with only alphanumeric characters from 3-12 characters.")
        
        return save_file_name.isalnum() and len(save_file_name) >= 3 and len(save_file_name) <= 12
    
    def check_save_name_available(self, save_file_name):
        print("Does file exist: " + str(path.exists(self.INIT_PATH + save_file_name + ".csv")))
        return not path.exists(self.INIT_PATH + save_file_name + ".csv")
        
    def create_experience_replay_file(self, save_file_name):
        print("File name is valid and not in csv storage. Creating experience replay file...")
        print("Configuring current experience below...")
        print(self.experience_replay_array)
        with open(self.INIT_PATH + save_file_name + '.csv', 'w', newline='') as csvfile:
            filewriter = csv.writer(csvfile, delimiter=',')
            print(len(self.experience_replay_array))
            filewriter.writerow(['B', len(self.experience_replay_array)])
            for i in range(len(self.experience_replay_array)):
                print(str(self.experience_replay_array[i]) + "\n")
                
                #converts index to csv
                filewriter.writerow(['I', i+1])
                
                #converts current position to csv
                cp_row = ['CP']
                for j in range(len(self.experience_replay_array[i][0])):
                    cp_row.append(self.experience_replay_array[i][0][j])
                filewriter.writerow(cp_row)
                
                # converts current state to csv
                for z in range(len(self.experience_replay_array[i][1])):
                    cs_row = ['CS']
                    for y in range(len(self.experience_replay_array[i][1][z])):
                        cs_row.append(self.experience_replay_array[i][1][z][y])
                    filewriter.writerow(cs_row)
                
                #converts action to csv
                filewriter.writerow(['A',self.experience_replay_array[i][2]])
                
                #converts reward to csv
                filewriter.writerow(['R',self.experience_replay_array[i][3]])
                
                for x in range(len(self.experience_replay_array[i][4])):
                    ns_row = ['NS']
                    for w in range(len(self.experience_replay_array[i][4][x])):
                        ns_row.append(self.experience_replay_array[i][4][x][w])
                    filewriter.writerow(ns_row)
                
            filewriter.writerow(['E'])
                
    def get_experience_replay_array(self):
        return self.experience_replay_array
    
    def print_experience_replay_array(self):
        print("Experience replay array:")
        print(self.experience_replay_array)