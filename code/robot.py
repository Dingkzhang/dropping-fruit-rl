# -*- coding: utf-8 -*-
"""
Created on Wed Jan  1 19:39:20 2020

@author: Dingie
"""

class Robot:
    is_game_ongoing = True

    
    def __init__(self):
        print("Cannot configure Robot agent...")
        return None

    def choose_action(self):
        return None
    
    def calculate_action(self):
        return None
    
    def get_action(self):
        return None
    
    def get_reward(self):
        return None
    
    
    def get_is_game_ongoing(self):
        return self.is_game_ongoing

    def set_is_game_ongoing(self, is_game_ongoing):
        self.is_game_ongoing = is_game_ongoing