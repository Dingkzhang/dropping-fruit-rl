# -*- coding: utf-8 -*-
"""
Created on Wed Jan  1 19:39:04 2020

@author: Dingie
"""



import numpy as np

class Play:

    agent_selected = None
    
    def __init__(self):
        return None


    def setup_game(self):
        print("Setting up Dropping Fruit simulation...")
        self.select_agent()
                
    def select_agent(self):
        is_valid_input = False
        while (is_valid_input == False):
            print("Please select Human (h) or Robot (r) agent...")
            agent = input("Agent: ").lower()
            if (agent == 'h' or agent == "human"):
                print("Test subject has selected Human (h) agent.")
                self.agent_selected = "h"
                is_valid_input = True
            if (agent == 'r' or agent == "robot"):
                print("Test subject has selected Robot (r) agent.")
                self.agent_selected = "r"
                is_valid_input = True
    
    def get_agent_selected(self):
        return self.agent_selected
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                